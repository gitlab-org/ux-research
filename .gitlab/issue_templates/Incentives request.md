<!--Use following title format: 
  "Incentives request for #(issue number of your research project)"-->

## To be completed by the requester
* [ ] Make a copy of the following Google Sheet: https://docs.google.com/spreadsheets/d/1moXi9orRAC2-nmlO-f0P0pnFsZu7PEspBrOjSl-Ooaw/copy
* [ ] Fill out the copied document where relevant.
* [ ] Place your Google Sheet in the shared [UX Incentives folder](https://drive.google.com/drive/folders/1rfHKQbG4-X7COhIg33ycL6FlmEoIw0ZE).
Navigate to Google Drive -> UX Research shared drive -> ReOps Team Folder -> UX Incentives Requests
* [ ] Paste the link to the completed, copied document here: 

## To be completed by the UX Research Coordinator
* [ ] Remove ReOps:: Triage label and apply your personal ReOps label (e.g. ReOps::Cait or ReOps::Jenn).
* [ ] Add request to [ReOps Tracking Sheet](https://docs.google.com/spreadsheets/d/1mFjo0hgNla_haDT5Xc7825WbZzaDtQ63BlmeWePRwm8/edit#gid=0).
* [ ] Pay users.
* [ ] Delete the copied document from Google Drive.

/confidential
/label ~"ReOps:: Triage" ~"UX ReOps Incentive Request" ~"Research Coordination"
/assign @cfaughnan 
/relate #<!--issue number here-->
