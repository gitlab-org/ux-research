<!-- 
This template is intended to be used for the quarterly execution of Navigation Research, conducted by the UX Research team.

It is best practice to start this process at least 1 month before the end of the current quarter. This will give you enough time to setup, collect data, and report out. The Navigation Survey is executed quarterly for New and Experienced GitLab.com users. 

Title the issue in the following format: FYXX-QX Navigation Quarterly Survey - Research Execution
 -->

#### Disclaimer 

**Please note: This Navigation Survey template is only designed to be used by the Navigation DRI within UX Research. If you would like to conduct research related to the Navigation survey, please contact the UX Research team in the `ux_research` Slack channel.**

# GitLab Navigation Survey (run every quarter)

### Survey Setup

* [ ] Duplicate the GitLab Navigation Survey template **\[FIXME: add link\]** in Google doc
* [ ] Work with ~"group::foundations" stakeholders directly in doc to add any additional questions
* [ ] Check with the UX Research team on any additional questions they may like to add
* [ ] Finalise the survey design in doc, and implement it in Qualtrics. (Note you can start with duplicating the previous Quarterly Navigation Survey in the shared UX Research Qualtrics account to save time.)

### Recruitment

* [ ] Complete the [Promotional Games workflow](https://about.gitlab.com/handbook/legal/ux-research-pilot/) to generate the abbreviated rules for the drawing.
* [ ] Publish the survey and update the Abbreviated Rules link in the "Sweepstakes" block in Qualtrics with the newly generated rules.
* [ ] Create a copy of the previous quarters Navigation Survey project in Rally
  * [ ] Request access to Rally if you don't already have access
  * [ ] Update the email template in the project with the new Abbreviated Rules link
* [ ] Find New and Experienced users from the data warehouse
  * [ ] Request access to Snowflake as an editor to be able to query, if you don't already have access.
  * [ ] User these queries to generate a list of user IDs and emails
    * [Query for New Users](https://app.snowflake.com/ys68254/gitlab/w8etxGDebTB#query)
    * [Query for Experienced Users](https://app.snowflake.com/ys68254/gitlab/w3P5zWUKJsr7#query)
* [ ] Import mailing lists into Rally
  * [ ] Re-run Snowflake queries for both cohorts to get refreshed lists
    * [ ] Download data, save it to your local file system, and rename it with the appropriate wave number
    * [ ] In Rally, go to the Participants tab and select "Add people", then select "Import people", and upload your list. 
    * [ ] Ensure that your data looks good and that the template columns are matched correctly and finish importing your lists.
    * [ ] Set the filters in the Participants tab to the following:
      * Contact Status contains Free to contact AND
      * Participant Status is not Opted Out AND
      * Participant Status is not Study Invite Sent
    * Note: If the target _N_ = 400 (200 New Users, 200 Experienced Users) and response rate is 1-2%, you will need to sample between 20,000 and 40,000. Start with waves of 6000 every couple days from New and Experienced users and adjust subsequent waves according to response rate, and the distribution.
* [ ] Distribute suvey in waves
  * [ ] Click "Select" in the Participants tab and enter 2000. **2000 is the maximum wave size for Rally at the moment. You can and should upload lists larger than this, but you can only distribute in 2000 person waves.** Based on the filters you established earlier Rally will only send to users who are free to contact. 
  * [ ] Distribute survey to yourself by selecting "Send test email to myself"
  * [ ] Review the email in your inbox and make any necessary amendments if needed.
  * [ ] Ensure you have selected "research@rally.gitlab.com" as the from address
  * [ ] Send the wave and repeate process as needed
* [ ] Close survey and cleanse data
* [ ] Conduct prize drawing and pay participants
  * [ ] Within the Survey Responses tab and click "Select", and enter 3 (or the number of winners for your sweepstakes). 
  * [ ] Click "Send incentive"

### Analysis 
1. [ ] Download [R](https://www.r-project.org/) and [R Studio](https://posit.co/downloads/)
1. [ ] Download the [Navigation Survey Scripts Repo](https://gitlab.com/NickHertz/navigation-survey-scriptsj) from GitLab
   - You can do this by clicking the Download icon to the left of the Clone CTA in the repo and select zip. It will download the files in the repo to the Downloads folder of your computer. You can move this folder within your local file system to wherever makes sense to you.
   - This is a copy specific to you and has no connection to the repo any longer. You cannot impact the repo, so do not worry. You can always redownload the repo if you have made changes.
   - Ask Adam Smolinski, Karen Li, or Nick Hertz for access.
1. [ ] Run `1-Read and Transform Qualtrics data.R` to ensure that you have met the sampling thresholds. You can use this script to monitor data collection.
1. [ ] Run `RUN THIS FIRST.R` to begin the transfer process to Google Slides

### Reporting & Sharing

* [ ] Create a slide deck (see example)
* [ ] Share it with the UX Researchers for feedback first before sharing it with the wider teams
* [ ] Create actionable insights issues (see example) and assign them to the relevant stakeholders 
* [ ] Create a separate sheet that contains participants who are interested in a follow-up conversation (Sample follow up sheet; [handbook overview of process](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/system-usability-scale/sus-outreach.html))

/label ~"UX Research"