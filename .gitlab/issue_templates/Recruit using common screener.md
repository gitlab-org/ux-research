<!--
Title format: "Recruiting using common screener for #(issue id of your research project)"
Example: "Recruiting using common screener for #123"
This template should be used when recruiting for a solution validation research using common screener.
-->

<!---To learn about the Research Recruitment process please review the Recruiting handbook page at https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/>--->
<!---To learn about the Common screener, go to the common screener page in the handbook at https://about.gitlab.com/handbook/product/ux/ux-research/recruiting-participants/common-screener/>--->

## To be completed by the Requester

#### What is the link to the research issue/epic you're recruiting for?
<!---If it's an issue, create a relationship by pasting the issue URL into the /relate quick action below.--->

/relate ISSUE-URL


#### Link the common screener coordination issue created by the UXR for your stage?
<!---To be able to use a common screener for recruitment, you should have already had a discussion with your supporting UXR and a coordination issue must be created before hand. Reach out to the UXR of your stage group if you're not sure of which coordination issue.--->



* Have you created a copy of the common screener in Qualtrics or in a Google Doc and shared with **both** UX Research Operations Coordinators?
     - [ ] cfaughnan@gitlab.com


#### When are you hoping to meet with users?
<!---For example: Over the next 2 weeks, within the next month, etc.--->

#### How long will your individual sessions be?
<!--- Once you provide the length of your study, the UX Research Operations Coordinator will be able to assign the correct incentive amount.--->

#### Would you like the UX Research Operations Coordinator assigned to your issue to shortlist for you? 
<!---Shortlisting means that the Coordinator assigned to your project will highlight potential individuals to you as they respond to the screener that may be a good fit for your study.--->
- [ ] Yes
- [ ] No 

#### Do you have any upcoming vacation time that may impact scheduling sessions for this study?


#### How do you plan to conduct the research?

- [ ] I want to meet with the participants over a zoom meeting 
    - [ ] Use this [template email](https://docs.google.com/document/d/1Wxq446E8YtddR46Q0FrT1lZz6UX-A0l6CKmziO0_K54/edit?usp=sharing) and send it out to selected participants with your Calendly link. Make sure your Calendly account is configured with the right Zoom link.
- [ ] I want to share a User-testing link
    - [ ] Follow the instructions below to create a link for your user-testing project

    <details><summary>Instructions</summary>
    
    | Create test | Choose test type | Opt to create a link | Customize invite |
    |-------------|------------------|----------------------|------------------|
    | From the top right corner dropdown, select `Create test` | On the screen that follows, select the test type | Under options to reach an audience, select `Create link` | Optionally, at the end customize the invite by adding gitlab assets that can be found [here](https://about.gitlab.com/press/press-kit/). Don't forget to preview the changes. |

    </details>

    - [ ] Use this [template email](https://docs.google.com/document/d/1Wxq446E8YtddR46Q0FrT1lZz6UX-A0l6CKmziO0_K54/edit?usp=sharing) and send it out to selected participants with a link to your user-testing project generated in the previous step.

-------

#### Checklist
- [ ] Create a draft of a common screener linked in the the common screener coordination issue created by the UXR for your stage, in a document or in Qualtrics.
- [ ] Have your teams and supporting UXRs take a look to provide feedback and confirmed that they'll be able to use your common screener? 
- [ ] Before you select / invite participants you need to ask the research coordinator to do a recruitment push to this common screener so that there will be participants for the next person
- [ ] Create a copy of the [gratuity spreadsheet](https://docs.google.com/spreadsheets/d/1MU5WRbOagDP-RMT-vhVO_9YvCc19m1CsDFFDde-iE8o/edit?usp=sharing) to track the payment. 
- [ ] Move your copied sheet to the [UX Incentives Request](https://drive.google.com/drive/folders/1rfHKQbG4-X7COhIg33ycL6FlmEoIw0ZE) folder in the shared UX Research drive.
- [ ] Paste the link to the completed, copied spreadsheet here:

<!--- Please note you do not need to open a seperate Incentives issue once the Recruiting issue is open for User Interviews.--->

------


## To be completed by the UX Research Operations Coordinator
* [ ] Unassign other Research Ops Coordinator & comment to let them know you'll be taking the request.
* [ ] Remove the ReOps::Triage label and replace with your own ReOps label.
* [ ] Add Study to [Research Ops Spreadsheet](https://docs.google.com/spreadsheets/d/1mFjo0hgNla_haDT5Xc7825WbZzaDtQ63BlmeWePRwm8/edit#gid=0)
* [ ] Sense check the screener or survey.
* [ ] Check that the requestor's Calendly link is set-up correctly.
* [ ] Distribute the screener or survey. Remember: With surveys, send to a sample of participants first.
* [ ] Schedule users [if required].
* [ ] Pay users
* [ ] Delete the copied spreadsheet from Google Drive (to reduce the PII footprint).
* [ ] Close study in Research Ops Spreadsheet



/label ~"UX ReOps Recruitment" ~"Research Coordination" ~"ReOps:: Triage"
/confidential
/assign @cfaughnan 
