<!--
Title format: "Recruiting for #(issue id of your research project)" Example: "Recruiting for #123"
-->

<!--
To learn about the Research Recruitment process please review the Recruiting handbook page at https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/>
-->

## To be completed by the Requester

#### What is the research project you're recruiting for?
<!--Create a relationship by using the '/relate URL' quick action below. -->

#### Who are your target participants?
<!--Add specific recruitment criteria, e.g. "SREs who use Kubernetes, a mix of GitLab and non-GitLab users, Assistive Technology users." -->

#### What is your target sample size?
<!--Speak with the UX Researcher if unsure. We recommend 5-8 per user group for usability test, interviews, 8-10 for diary study, 15-20 for card sorting, 30 for tree testing. -->

#### What is the link to your screener or survey?
<!--Screener or survey must be entered into Qualtrics before it can be distributed to users.

Need help getting started with your screener? Here's an example of a completed screener - https://gitlab.eu.qualtrics.com/jfe/preview/SV_cT80heuzGIlAdZH?Q_SurveyVersionID=current&Q_CHL=preview -->

* **(Required)** Screener in Google doc, with qualifying & disqualifying criteria clearly labeled: 
  * **(Required)** Tick the box if you've done the following. Note recruitment **cannot** proceed if the following steps are not completed:
    - [ ] The screener has gone through peer review with the UX Research team.
    - [ ] **(If ReOps to set up the screener)** The screener doc is shared with the UX Research Operations Coordinator (cfaughnan@gitlab.com).
* Screener in Qualtrics (if exist already):
* Screener in Rally (if exist as part of the study):

#### When are you hoping to meet with users?
<!---For example: Over the next 2 weeks, within the next month, etc. Remember that recruitment timelines have a minimum of 2 weeks depending on criteria.--->

#### How long will your individual sessions be?
<!--- Once you provide the length of your study, the UX Research Operations Coordinator will be able to assign the correct incentive amount.
--->

#### Does the Research DRI have any upcoming vacation time that may impact scheduling sessions for this study?

## Checklist Before Launch

**Requester To-Dos**
* [ ] Comfirm with the UX Researcher that the recruitment criteria described here reflect your agreement.
* [ ] Apply ~"ReOps::ready for triage" label to this request.

**UX Research Operations Coordinator To-Dos**
* [ ] Unassign other Research Ops Coordinator & comment to let them know you'll be taking the request.
* [ ] Remove the ~"ReOps::ready for triage" label and replace with the ReOps grading level.
* [ ] Add Study to [ReOps Metrics Tracker](https://docs.google.com/spreadsheets/d/1oL_SpHMs_iDJrU94a1KnLBB668ieDgeXfJNoQXhHmWs/edit#gid=0)
* [ ] Sense check the screener or survey.
* [ ] Comment here once the above actions are done, and inform the Requester to continue with next steps.

## Checklist to Launch the Recruitment

**Requester To-Dos**
* **For Grade 1-2 level requests:**
  * [ ] Follow the recruitment self-serve guide.
  * [ ] If applicable, tag your UX Researcher for incentive processing in Rally.

**UX Research Operations Coordinator To-Dos**
* **For Grade 3-4 or ReOps Assisted level requests:**
  * [ ] Distribute the screener or survey. Remember: With surveys, send to a sample of participants first.
  * [ ] Schedule participants [if required].
  * [ ] Pay participants.

## (If recruitment is done outside of Rally) Checklist for Incentive Processing 

**Requester To Dos**
* [ ] [Make a copy of the participant reimbursement spreadsheet](https://docs.google.com/spreadsheets/d/1moXi9orRAC2-nmlO-f0P0pnFsZu7PEspBrOjSl-Ooaw/copy) and rename the copy to match the recruiting request issue (Please use the following naming convention: <Recruiting Issue ID# - Recruitment Request> Example: _Recruiting Issue 1234 - Recruitment Request_).
* [ ] Follow the instructions in the copied spreadsheet.
* [ ] Move your copied sheet to the [UX Incentives Request](https://drive.google.com/drive/folders/1rfHKQbG4-X7COhIg33ycL6FlmEoIw0ZE) folder in the shared UX Research drive.
* [ ] Paste the link to the completed, copied spreadsheet here:

<!--- Please note you do not need to open a separate Incentives issue once the Recruiting issue is open for User Interviews.
--->

**UX Research Operations Coordinator To-Dos**

* [ ] Pay participants in the spreadsheet.
* [ ] Delete the copied spreadsheet from Google Drive (to reduce the PII footprint).
* [ ] Close study in Research Ops Spreadsheet

## (Optional - for collaborative research projects) Participants Table
| Participant |  Invite? |Scheduled | Interviewed | Thank you gift sent? | Additional Notes |
| ------ | ------ | ------ | ------ |  ------ | ------ |
|  |  |  |  |   |  |
|  |  |  |  |   |  |
|  |  |  |  |   |  |
|  |  |  |  |   |  |
|  |  |  |  |   |  |


/label ~"ReOps" ~"ReOps Recruitment"
/confidential
