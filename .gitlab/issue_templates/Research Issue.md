<!-- This template is for any type of research. Fill in as much informatoin as you can before discussing with the researcher for your group or area (https://about.gitlab.com/handbook/product/categories/) If there is no one listed or you're unsure, assign it to the UX Research Manager (@karenyli). 
Leaving an issue unassigned means it may go unnoticed by the UX Research Team.
-->

#### What did we learn?

<!-- Add information for this section after the research is complete. -->

| Results |
| ------ |
| `2-3 sentences to summarize the results` |
| `Link to Reoport, Dovetail project` |

---

<!-- Please answer the below questions to the best of your ability.-->

## Part 1. The Basics 

### 1. Context and Goals

#### Problem(s)
<!--What is the problem or opportunity that prompted this research? It could be anything that highlights the need for this work, e.g. risks, links to notes, opportunity canvases.-->

#### Goal(s)
<!--What is the goal of this research? Describe what you aim to learn or achieve through this research, e.g. benefits, relevant KRs, yearlies, or strategies.-->

#### What Next
<!--What decision(s) will this research inform? Explain how the research findings will be used and by whom.-->

#### Intended Impact Type
<!--What is the type of **main** impact intended? Select the **ONE** option that best describes your goal-->
- Agreed impact type:

<details>
<summary>
:star2: Click to see impact type selections
</summary>

<table border="1" cellpadding="10">
<tr>
<th>Impact on Decisions</th>
<th>Impact on Knowledge</th>
<th>Impact on UXR maturity</th>
</tr>
<tr>
<td>

- [ ] Changes in strategy/plan
- [ ] Changes in product/design

</td>
<td>

- [ ] Knowledge gaps identified/filled
- [ ] User's perception of our product captured
- [ ] Next big opportunity identified

</td>
<td>

- [ ] Improvement on UXR process efficiency
- [ ] Improvement on UXR quality & reliability

</td>
</tr>
</table>
</details>

#### Timeline
<!--What timescales do you have in mind for the research? Include key milestone or the target quarter.-->


### 2. Research Scope and Support Needs

####  Research Question(s)
<!--What research questions are you trying to answer?-->

#### Hypotheses
<!--What hypotheses and/or assumptions do you have?-->

#### Target User Group
<!--Which persona, user segment, or customer type is this research aimed at or likely to impact?-->

#### Relevant Works
<!--What, if any, relevant prior research or relevant work already exists?-->

### 3. End of Part 1 Checklists

**Issue Creator To-Dos**
- [ ] Assign the issue to yourself and the UX Researcher of your product group or area.
- [ ] Discuss the research needs with the UX Researcher.

<details>
<summary> 

**UX Researcher To-Dos**

</summary>

- [ ] If the research is not needed, apply the ~"closed::will not do" label, and close the issue.
- [ ] If the research is needed, work with the Issue Creator on the rest of the issue, and put this project through the global prioritisation process.
</details>

## Part 2. Priority and Support Levels
<!--Complete this to the best of your knowledge, or work with a UX Researcher on this later.-->

### 1. Priority Level

- Agreed Priority Level: 

<details>
<summary>

:star2: **Click to expand, and answer some questions to help UXR assess priority.**

</summary>

_Select **ONE** answer per question._

<table border="1" cellpadding="10">
<tr>
<th>1. What is the business impact if we don't do this research?</th>
<th>2. How many users/customers would this affect?</th>
<th>3. Is this blocking any immediate decisions?</th>
</tr> 
<tr>
<td>
        
- [ ] 1. Critical Impact - major risks
- [ ] 2. High Impact - significant opportunity
- [ ] 3. Moderate Impact - helpful insights
- [ ] 4. Low Impact - nice to have
- [ ] 5. Minimal Impact - no clear case

</td>
<td>

- [ ] 1. All users/critical segment
- [ ] 2. Large user segment
- [ ] 3. Medium user segment
- [ ] 4. Small user segment
- [ ] 5. Very few users

</td>
<td>

- [ ] 1. Blocking critical launch
- [ ] 2. Blocking important decision
- [ ] 3. Input needed soon
- [ ] 4. Future planning
- [ ] 5. No specific decision

</td>
</tr>
<tr>
<th>4. What is the timeline flexibility?</th>
<th>5. Are there alternative ways to get this information?</th>
<th>6. How does this align with our strategic goals?</th>
</tr>
<tr>
<td>

- [ ] 1. This quarter
- [ ] 2. Immediate (this month)
- [ ] 3. Within 6 months
- [ ] 4. Within 9 months
- [ ] 5. No timeline pressure
        
</td>
<td>

- [ ] 1. No alternatives exist
- [ ] 2. Limited alternatives
- [ ] 3. Some alternatives
- [ ] 4. Some good alternatives
- [ ] 5. Many alternatives

</td>
<td>

- [ ] 1. Core strategic priority
- [ ] 2. Aligns with quarterly goals
- [ ] 3. Support team objectives
- [ ] 4. Indirect alignment
- [ ] 5. No clear alignment

</td>
</tr>
</table>
</details>

### 2. UXR Support Level

- Agreed UXR Support Level: 

<details>
<summary>

:star2: **Click to expand, and answer some questions to help UXR assess support level**

</summary>

_Select **ONE** answer per question._

<table border="1" cellpadding="10">
<tr>
<th>1. The type of research the project falls into.</th>
<th>2. Can this research be supported someone other than a UX Researcher?</th>
<th>3. Is this project being requested by a Product team with Product Design support?</th>
</tr>
<tr>
<td>
        
- [ ] 1. Foundational
- [ ] 2. Problem Validation
- [ ] 3. Solution Validataion
        
</td>
<td>
        
- [ ] 1. Yes
- [ ] 2. Somewhat
- [ ] 3. No
        
</td>
<td>

- [ ] 1. Not applicable for this research (study created/led by UX Research)
- [ ] 2. Requesting Product team has Product Designers
- [ ] 3. Requesting Product team does not have Product Designers

</td>
</tr>
<tr>
<th>4. Does this project involve multiple studies or methodologies?</th>
<th>5. Will this support skill development for the team or refine a process if a UX Researcher is involved?</th>
<th>6. What level of confidence or knowledge do you have in the proposed solution or area of focus?</th>
</tr>
<tr>
<td>
        
- [ ] 1. Yes
- [ ] 2. No
- [ ] 3. I don't know
        
</td>
<td>

- [ ] 1. Yes
- [ ] 2. Somewhat
- [ ] 3. No

</td>
<td>

- [ ] 1. High
- [ ] 2. Medium
- [ ] 3. Low

</td>
</tr>
</table>
</details>

### 3. Who will be leading the research, and who will be supporting?

- Research DRI: 
- Supporting UX Researcher: 

## Part 3. Research Documents and Methods

<!--Provide links to the following documents, if applicable, or any methods and plans.
Find all standardised screening question in this repository: https://docs.google.com/document/d/1v8_a3DH9UKhakn61Z1vFXUT-IOAPC6-FwJegaquYUS4/edit?usp=sharing
-->

- Recruitment screener:
  - [ ] Peer reviewed by UXR
- Research plan:
  - [ ] Peer reviewed by UXR
- Interview script / Unmoderated test script: 
  - [ ] Peer reviewed by UXR
- Prototype: 
  - [ ] Preflight checked (ensuring the prototype is functional and free of major issues)

### Final Checklist

**Research DRI To-Dos**
- [ ] Ensure your research documents are reviewed by the UX Research Supporter.
- [ ] If internal or external recruitment is needed, create a recruiting request issue **ONLY AFTER** your screener has been reviewed by the UX Researcher.
- [ ] Share your research findings in the [#ux_research_reports](https://gitlab.enterprise.slack.com/archives/C0464LVBGKX) channel using the template below.

<details>
<summary>
:star2: Click to see slack reporting template
</summary>

:gitlab: **Project Title** :gitlab:

A brief description of the project.

**Key Insights:**

* Add your insight here.

**For More Information:**

* :google_drive: Link to the research report
* :tanuki-heart: Link to research issue (this issue)
* :dovetail_logo: Link to dovetail recording (if applicable)

**Next steps:**

* Add your next step here.

</details>

/label ~devops:: ~category: ~group::
/label ~"UX research"
/label ~"UX Research Backlog"
/confidential 