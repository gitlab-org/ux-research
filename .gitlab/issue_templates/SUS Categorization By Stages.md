<!--Use following title format: 
  "Accurately categorize SaaS SUS verbatim by stages - FYXXQX"
  or
  "Accurately categorize SaaS & Self-Managed SUS verbatim by stages - FYXXQX"-->

#### What’s this issue all about?

[This Q4 UXR KR](https://gitlab.com/gitlab-org/ux-research/-/issues/1635 "FY22 Q4 UXR KR3: Establish a delivery process for SUS detailed verbatim findings for stages"), details a process to share out verbatim derived from our SUS survey results to specific associated stages. To do this, we first must make sure the data is categorized accurately by subject matter experts, then shared out to stages via the relevant Slack channels by the UX Researchers.

#### What's needed from each of you:

You have each been identified as subject matter experts for your given stages. Please help by completing the following steps:

<!--If it is Q1 or Q3, keep step 1 below. If Q2 or Q4, remove line about "and `SM_Usability Verbatim`". Also, insert the file link for the SUS Google Sheet in step 1.-->

1. Go into `Usability Verbatim` and `SM_Usability Verbatim` tab [within this file](ADD NEW FILE HERE).
2. Read each verbatim in **Column A**.
3. If a verbatim clearly pertains to one of your stages, **categorize it in Column K** by selecting the relevant stage. The verbatim may mention multiple stages, if so please utilize the **secondary stage in Column L**.
4. Document that you've completed verbatim categorization in the table below with a :white_check_mark:.
5. Modify the best Slack channel(s) for the UXRs to share the categorized verbatim to within the table below, if different than presented.
6. Un-assign yourself from the issue. Done!

<!--Review and update PDMs and UXRs within the table below to ensure they are accurate-->

<!--Some stages may have been eliminated or may have gotten combined since last quarter. Take time to review and update parts of the table and SUS results Google Sheet before creating this issue.-->

| Stage | PDM | Completed verbatim categorization? | Best Slack channel(s) to communicate findings? | UXR | UXR communicated out via Slack? |
|-------|-----|------------------------------------|------------------------------------------------|-----|---------------------------------|
| Manage | @chrismicek  |  | #s_manage | @aknobloch  |  |
| Plan | @jackib  |  | #s_plan | @dteverovsky  |  |
| Create | @andyvolpe  |  | #s_create | @leducmills |  |
| Ecosystem, Foundations, Integrations | @chrismicek  |  | #g_manage_integrations, #g_manage_foundations | @aknobloch |  |
| Package | @rayana |  | #s_package, #ops_section | @enf |  |
| Verify | @rayana  |  | #s_verify, #ops_section | @enf  |  |
| Release | @rayana |  | #g_environments, #ops_section | @wleidheiser |  |
| Configure | @rayana  |  | #g_environments, #ops_section | @wleidheiser |  |
| Monitor | TBD |  | #s_monitor, #ops_section | @wleidheiser |  |
| Secure | @jmandell  |  | #s_secure | @moliver28 |  |
| Govern | @jmandell  |  | #s_govern | @moliver28 |  |
| Growth | TBD |  | #s_growth | @NickHertz  |  |
| Fulfillment | @jackib  |  | #s_fulfillment | @NickHertz  |  |
| Enablement | @chrismicek  |  | #s_enablement | @wleidheiser |  |
| ModelOps | @jmandell  |  | #s_modelops | TBD |  |
| Anti-Abuse | @jmandell  |  | #g_anti-abuse | TBD |  |

Thank you! :sparkles:

#### For UX Researchers:

* [ ] Notify UX Researchers when PMs have started to categorize

Here is a template message you can use to post your stage's verbatim to the appropriate slack channel. Please locate your stage's sheet in step 1 above. When you have found your stage's sheet copy the link from the url bar to share that specific page.

> Hello :wave: - We just completed analyzing the Q# FY## System Usability Scale (SUS) data! I wanted to share the verbatim that's relevant to us in the `fill in stage name here` stage. Here's a sampling:
>
> * `Stage UXR to paste example in italics`
> * `Stage UXR to paste example in italics`
>
> You can view the remaining **`#`** quotes here --> `link to the Sheet, on the specific tab`
>
> Let me know if you have any questions or if you're interested in pursuing some of these!

CC: @vkarnes @clenneville @NickHertz @asmolinski2

/assign @chrismicek
/assign @jackib
/assign @andyvolpe
/assign @rayana
/assign @jmandell
