<!-- 
This template is intended to be used for the quarterly execution of responder outreach for the User Satsifaction (USAT) research.

It is best practice to start this process after USAT results have been presented for the quarter. USAT is executed quarterly within the Product division, on the GitLab product itself. 

Title the issue in the following format: Responder Outreach - FYXX QX - User Satisfaction (USAT) Survey
-->

Hello Product Managers and Product Designers! Please take the time to sign up, coordinate, and reach out to User Satisfaction (USAT) users who opted to chat with us!

#### How can I sign up?

You can sign up (or assign your Group Product Managers, Product Managers, Product Designers, Customer Success Managers) by using the following **Google Sheet**:

- `FYXX QX USAT Follow-up Users Sheet`

When you find one or more survey respondents you want to contact:

- [ ] Add your name to `Product Team Member Assignee` or `CSM Assignee` (Columns K & L)
- [ ] Update `Status` (Column M) by selecting an item from the dropdown menu
- [ ] Add any other details or updates to `Notes` (Column N)

Please note access is restricted to only `product@gitlab.com` and `timtams@gitlab.com` due to privacy of user contacts.

#### What is the process to schedule interviews with survey respondents?

The process to follow is the same as last quarter and can be found in the product handbook: [USAT Responder Outreach](https://handbook.gitlab.com/handbook/product/product-processes/#usat-responder-outreach).

- [ ] There is a [template](https://gitlab.dovetail.com/data/uGqDweiCrETTBRuXKptAC) available to use for conducting interviews within Dovetail. Please **make a copy of the template** and place it under the `USAT` column when adding your interview data to the project.
- [ ] When you conduct the interviews, please record the sessions and post them to the [PNPS/SUS/USAT Follow Up project in Dovetail](https://gitlab.dovetailapp.com/projects/36nmGVKvkaT7SGMXtUeHVg/v/70xPTo5RzTRZnCNEVz1fWH). 
- [ ] Add the link to the session recording in Dovetail to the `Notes` column (Column N)
- [ ] In order for us to track the impact of these interviews, please add the following label (`USAT::Responder Outreach`) to any existing or new issues that you create.

**Important note**: We do not want to overwhelm users with multiple contacts. If the person you want to talk to has already been signed up for by someone else for outreach, coordinate to join your questions and/or outreach session. Thank you!

#### **To Do (for UX Research only)**

`[USAT survey issue](Add Link Here)`

- [ ] Add `:recycle: Retrospective Thread` as a comment
- [ ] Create the USAT follow-up user list with restricted access to only `product@gitlab.com` and `timtams@gitlab.com` ([sample](https://docs.google.com/spreadsheets/d/19Hm4ZhtH3YXEyG0p43HlLaSg5g84MIWorZSjHchvOpg/edit?usp=sharing))
- [ ] Update content and all document links in issue description as needed
- [ ] Add due date: last day of the current fiscal quarter
- [ ] Tag in `@gl-product-pm` in issue comment to action Product Managers and Product Designers
- [ ] Add in `@paintedbicycle-gitlab` in issue comment to action Paper Cuts Team
- [ ] Add in `@spatching` and `@bsinq` in issue comment to action Customer Success Managers
- [ ] Add in `@seanhall` and `@sdevries2` in issue comment to action Pricing team
- [ ] Send a message to VP of UX that the Responder Outreach issue is available for this quarter and request that PLT action PMs and PDs on this issue. Ask VP of UX to include this update in the PLT weekly meeting agenda.
- [ ] 1 week prior to due date:
  - [ ] Check user list to see if team has actioned sufficiently, and if not, ping `Product Team Member Assignees` and `CSM Assignees` listed in the sheet to action
- [ ] Remind team members who have conducted interviews to add the `USAT::Responder Outreach` label on new or existing issues
- [ ] Update this [issue template](https://gitlab.com/gitlab-org/ux-research/-/tree/master/.gitlab/issue_templates) with any items from the Retrospective thread
- [ ] Review and close issue on due date

/label ~"UX research"
/confidential 
