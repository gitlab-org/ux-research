<!-- 
This template is intended to be used for the quarterly execution of the User Satisfaction (USAT) survey. Note: Customer Success conducts Customer Satisfaction (CSAT), which is a separate survey effort.

Title the issue in the following format: User Satisfaction (USAT) Survey: FY20XX QX
-->

#### Disclaimer 

**Please note: This USAT Survey template is only designed to be used by the USAT DRI within UX Research. If you would like to conduct research related to the USAT survey, please contact the UX Research team in the `ux_research` Slack channel.**

#### What did we learn?

| Results |
|---------|
| `2-3 sentences to summarize the results` |
| `Link to report in Google Slides` |

---

#### What's this issue all about? (Background and context)

* Within the Product division, we have adopted [User Satisfaction Score (USAT)](https://www.qualtrics.com/uk/experience-management/customer/what-is-csat/) and are conducting this survey on a quarterly basis.
  * We previously surveyed customers using the Net Promoter Score (NPS), which we moved away from starting in FY25 Q1 ([see proposal deck](https://docs.google.com/presentation/d/18FYO0UoUQi__7hPnD0uhVmeJrN16n23OISQnom0eRE8/edit?usp=sharing)).
* We are using USAT because it allows us to:
  * Measure satisfaction directly vs. indirectly.
  * Connect satisfaction ratings and open ended responses back to changes in our product.
  * Compare and contrast USAT survey data against our other company wide metrics (SUS).

#### What are the overarching goals for the research?

* Track scores for the USAT by comparing them over time. Our [target USAT score is 76%](https://gitlab.com/gitlab-org/ux-research/-/issues/3087).
* Identify top themes for this quarter within the open ended USAT questions.

#### What business decisions will be made based on this information?

* This USAT score is used within [UX as a KPI](https://handbook.gitlab.com/handbook/product/ux/performance-indicators/#regular-performance-indicators) to track how satisfied our customers are with GitLab, the product.
* The user feedback from the USAT will help Product teams know what is working well and what could be improved.

#### What, if any, relevant prior research already exists?

* This [Google Drive folder](https://drive.google.com/drive/u/0/folders/1ia-SJujFRWgOpnBGUdLE6gAWE8ldf0DM) has information on current and past USAT research.

#### What timescales do you have in mind for the research?

* Looking to start and finish this research within the first month of the quarter.

#### Who will be leading the research?

* `Add UX Research DRI here`

#### USAT Survey Overview

The USAT consists of a series of survey waves distributed through Rally to customer email addresses with the intention of:

1. Delivering an overall USAT score by end of the quarter, so we can track scores quarterly.
2. Delivering scores by plan type, ensuring we get a sample that is representative of plan type breakdown.

**Requirements & Methodology:**

For the USAT survey, we will use the following recruiting criteria:

* Paid users (Premium & Ultimate) on Gitlab.com and Self-Managed GitLab.
   * Includes end users of the product.
* Survey eligible users with the goal of achieving representative breakdown by plan type within +/- 3% accuracy (excluding Self-Managed users because it has been difficult to capture enough responses from Self-Managed users in past survey research).
* Active GitLab accounts with activity in the past 60 days.
* No user who was contacted about the USAT survey within the past 12 months gets sent a USAT or CSAT survey. We will coordinate with Customer Success in Q2 and Q4 to state who we are planning to contact, so they are informed and can remove those individuals from their survey distributions.

# USAT Email Template

Subject line: `How can we improve your satisfaction with GitLab?`

![Screenshot 2024-04-10 at 3.10.24 PM.png](/uploads/b088c547ec189116fa16aea26718df64/Screenshot_2024-04-10_at_3.10.24_PM.png){width="410" height="436"}

# Survey Questions

Note: We ask about "GitLab (the product)" because we've gotten feedback about things that go beyond product specific feedback (for example: interview process, brand perception, etc.)

We are using the following survey questions:

`How satisfied are you with GitLab (the product)?`

* Very dissatisfied
* Dissatisfied
* Neutral
* Satisfied
* Very satisfied

`Why are you satisfied or dissatisfied with GitLab (the product)? (Optional)`

* (Open ended text field)

`How could your satisfaction be improved with GitLab (the product)? (Optional)`

* (Open ended text field)

`Would you be willing to talk with someone at GitLab about your feedback? This would be for research purposes, not for sales or marketing.`

* [ ] Yes, I would be willing to discuss my feedback
  * Please enter the email address where you would like to be contacted (Open ended text field)
* [ ] No, I would not like to be contacted

# How USAT is calculated

Note: Please see the `Resources` section at the bottom of this issue for the `Analysis Sheet` we use for calculating USAT results.

* (Number of satisfied customers / number of survey responses) x 100 = % of satisfied customers
  * Note: Satisfied customers = participants who indicated `Satisfied` or `Very satisfied`
* USAT scores are usually expressed as a percentage scale: 100% being total customer satisfaction, 0% total customer dissatisfaction.

# Tools & Process

1. Pull a list of users for the quarter using [this query](https://docs.google.com/document/d/1XSABfApXJY_VHm7Q9j4V64xsMNkSorb5SWcLn-b4Tfk/edit). Those users are users on a paid .com plan.

### Using Google Sheets

1. Check the list against the list of emails sent in the [previous quarters for the past 12 months](https://docs.google.com/spreadsheets/d/1Q9xOh5L9QJGdW9tq89rcADvo5jozRxdo7KiO2z0HLAU/edit#gid=0), scrubbing anyone previously contacted.
   1. Remove users who have a GitLab email address or have a bot related email.

### Using Tableau

1. [Look up the overall percentage breakdown by plan](https://10az.online.tableau.com/#/site/gitlab/views/DraftTDLicensedUsersbyProductRatePlanName/TDLicensedUsersbyProductRatePlanName?:iid=1). Our end goal will be to achieve a sample breakdown that roughly matches our population breakdown (-/+3%).

### Using Rally (GitLab.com users only)

1. Send out waves of approximately 5,000 emails using the email template in Rally called `USAT Email Template`. The email needs to use the following subject line: `How can we improve your satisfaction with GitLab?`
   1. Create filters to contact specific users for each wave (see screenshot below for details)
      1. ![Screenshot 2024-05-09 at 12.57.55 PM.png](/uploads/06d93970c9d8d056abd02c3c30ba4a87/Screenshot_2024-05-09_at_12.57.55_PM.png)
      2. Include additional filters to sort by Premium or Ultimate users
      3. Use the `Select` dropdown to pull a random selection of users for your sends - to match the population percentages
2. After each wave:
   - Pull a list of completed responses and calculate the current plan breakdown
   - Adjust the next wave to compensate for the current response breakdown, adding or subtracting the quantities of a product tier and plan for that wave as needed
3. Send out additional waves until we reach **at least 384 completed responses**
   1. 384 responses is the [recommended sample size](https://www.qualtrics.com/blog/calculating-sample-size/) with a 95% confidence interval, population of approximately 2 million+ paid users (current number of licenses across .com, Self-Managed, and Dedicated), and 5% margin of error.

# Estimated Research Timeline

- Data collection: 4-6 weeks
  - 4 hours of initial preparation work
  - 1 hour per week distributing each wave
- Data analysis: 1 week
  - 16 - 24 hours of cleaning data, categorizing verbatims, creating graphs and writing report
- Editing report draft: 1-2 weeks
  - 3 days of async review with UX Research team
  - 2 hours of responding to stakeholder feedback and updating report
  - 1 hour of distributing research across different organization channels (e.g., Slack, GitLab issues, etc.)

# Next Steps

#### Issue Setup

* [ ] Add assignee from UX Research to issue
* [ ] Add due date to issue

#### Recruitment

* [ ] **(About 1-2 weeks in advance of data collection)** Create a new Access Request (see [handbook](https://handbook.gitlab.com/handbook/business-technology/end-user-services/onboarding-access-requests/access-requests/)) to request access to Snowflake (example [issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/28716 "William Ryan Leidheiser, Snowflake, Staff UX Researcher"))
  * [ ] Request appropriate level of access in the issue (ask for access to PROD.RESTRICTED_SAFE_COMMON and Snowflake_Analyst_Safe)
  * [ ] Get approval from VP of UX within the issue comments
* [ ] Make files for PNPS this quarter
  * [ ] Open new folder in [Google Drive](https://drive.google.com/drive/u/0/folders/1ia-SJujFRWgOpnBGUdLE6gAWE8ldf0DM) and use following naming convention for folder name: `FYXX QX USAT`
  * [ ] Copy files in [Templates folder within Google Drive](https://drive.google.com/drive/u/0/folders/1c03b1cyUP9mYX6TQffm1Bk2FTKkrltI1)
  * [ ] Move copied files into new folder for this quarter
* [ ] Open up `FYXX QX USAT User Sheet`
  * [ ] Review latest number of licenses in this [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DraftTDLicensedUsersbyProductRatePlanName/TDLicensedUsersbyProductRatePlanName?:iid=1) and post numbers in `num` column
* [ ] Use [USAT query](https://docs.google.com/document/d/1XSABfApXJY_VHm7Q9j4V64xsMNkSorb5SWcLn-b4Tfk/edit) in Snowflake to generate a list of user IDs and emails
  * [ ] Run query (adjust limit on number of emails in the query as needed)
  * [ ] Download CSV file
  * [ ] Copy/paste user id, email, and plan data from CSV file to [USAT previous contact list sheet](https://docs.google.com/spreadsheets/d/1Q9xOh5L9QJGdW9tq89rcADvo5jozRxdo7KiO2z0HLAU/edit?gid=1667678518#gid=1667678518) in the `current survey distribution` tab
  * [ ] Filter by `N/A` in the `Contact by` column
  * [ ] Determine if you have 35,000-40,000 usable emails (if not, move more emails to the sheet)
  * [ ] Copy/paste `user id`, `email`, and `plan` columns to `Master` tab of `FYXX QX USAT User Sheet`
* [ ] Create recruiting request issue for Research Ops Coordinator to help with distributing USAT to Self-Managed users
  * [ ] Contact Research Ops Coordinator to make them aware of upcoming survey distributions before sending out surveys to users
  * [ ] **(May need to be repeated)** Ask for Research Ops Coordinator to identify Self-Managed users in Marketo and send them the survey
* [ ] **(Only for Q2 and Q4 data collection)** Compile list of users we will contact for the USAT and include Google Sheet in [Google Drive folder](https://drive.google.com/drive/folders/1ecM1ptkaNfNA0Im9wHulcl64QrCBOybQ)
  * [ ] Create issue for all Self-Managed waves contacted through Marketo to send to Customer Success ([see example issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/9872))
* [ ] **(Only for Q2 and Q4 data collection)** Contact Customer Success team (`Brittney Sinq`, `Sarah Schuster`, `Michelle Harris`, `Keith Mattes`) to make them aware of our upcoming survey distributions before sending out surveys to users
* [ ] Duplicate USAT survey in Qualtrics and rename to current fiscal year/quarter
  * [ ] Duplicate for .com and SM
* [ ] Create Rally project for this quarter's USAT survey
  * [ ] Connect .com and SM USAT survey in Qualtrics to Rally project(s)
* [ ] Import mailing list into Rally
  * [ ] Download `Master` tab from `FYXX QX USAT User List` Sheet as CSV
  * [ ] Upload CSV file in Rally project for USAT
  * [ ] Clean list (remove bots, duplicate users)
*  x] Launch surveys via USAT email template in Rally
  * [ ] Contact GitLab Dedicated CSMs to have them distribute emails/Qualtrics links
  * [ ] Send waves of up to 5,000 emails each wave until you hit target of _N_ = 384 (aim for +/- 3% from population percentages for .com Premium / Ultimate; aim for about 50-100 paid Self-Managed users since they are harder to obtain)
  * [ ] Follow instructions for contacting users in the `Using Rally` section of this issue
  * [ ] Review survey answers after each wave to track how close sample percentages are to the population percentages
* [ ] Close survey(s) in Qualtrics
  * [ ] GitLab.com survey
  * [x] GitLab self-managed survey
* [ ] Download survey data in Qualtrics
  * [ ] On the `Data & Analysis` tab in Qualtrics, go to `Export & Import` and select `Export Data`
  * [ ] Select `CSV` file, download all fields, and use choice text
  * [ ] Remove rows 2 and 3 of the Qualtrics CSV file
  * [ ] In Rally project, go to `survey responses` tab and select the following columns (first name, last name, email, plan, gitlab user ID, SaaS or Self-Managed, Participant ID)
  * [ ] Make sure both CSV files have the same column name for ID used in Rally (call it `rallyParticipantId`)
  * [ ] Ask `Nick Hertz` to help join separate CSV files
  * [ ] Copy/paste information from CSV file into each column `Completed` tab of this quarters' `Analysis Sheet`
* [ ] Fill in missing data for those who didn't finish entire survey (i.e., these participants will not have an email or plan listed in the combined data sheet)
  * [ ] Go to the `In Progress` tab in Rally, export data into CSV, and filter full dataset by `Finished = False` to fill in .com users that do not have an email or plan listed.
  * [ ] Use `rallyParticipantId` or `Participant ID` from both datasets to fill in missing information.
* [ ] Clean survey data (see [handbook page](https://handbook.gitlab.com/handbook/product/ux/ux-research/surveys/how-to-identify-low-quality-data-when-designing-and-reviewing-your-survey/) for guidance) by putting responses in `Participant Data Removed` tab of `Analysis Sheet`
  * [ ] Review `Q_RecaptchaScore`, `Q_RelevantIDDuplicateScore`, and `Q_RelevantIDFraudScore`
  * [ ] Find whether there are duplicate `IPAddress`
  * [ ] Look at `Duration (in seconds` to see whether respondents completed the survey in 0-5 seconds
  * [ ] Check for inconsistent survey responses (gave `Very Satisfied` rating, but had strong negative opinion of GitLab)
  * [ ] Consider if verbatims were likely written by a bot or AI
* [ ] **(Only for Q2 and Q4 data collection; Once survey goal is complete for this issue)** Pull final list of .com users contacted through Rally, share with Customer Success in [Google Drive folder](https://drive.google.com/drive/folders/1ecM1ptkaNfNA0Im9wHulcl64QrCBOybQ), and notify Customer Success team
* [ ] Pull final list of .com users contacted through Rally, update [previous contact list sheet](https://docs.google.com/spreadsheets/d/1Q9xOh5L9QJGdW9tq89rcADvo5jozRxdo7KiO2z0HLAU/edit#gid=0)
* [ ] Update links to user list used, all written and raw data/analysis sheets in this issue description
  * [ ] Please create and maintain all USAT files in this [shared folder](https://drive.google.com/drive/u/0/folders/1ia-SJujFRWgOpnBGUdLE6gAWE8ldf0DM)

#### Analysis, Reporting, & Sharing

* [ ] Add final cleaned data to `Analysis` sheet and populate each tab within the sheet
  * [ ] Use the themes and corresponding definitions in the `Theme Glossary` tab to fill out all themes connected to `Open Ended Feedback`
  * [ ] Once all verbatims have been categorized, copy over theme columns into `USAT Themes` tab to create graphs
* [ ] Contact `Paul Wright` from Papercuts team to review and check verbatims identified as the `Potential Papercut` theme in `Open Ended Feedback` tab of Analysis sheet
* [ ] Add data from `Analysis` sheet to `Open Responses` and `Followup Users` sheets
  * [ ] Move over only those who have agreed to a follow-up interview into the sheet
  * [ ] Fill out dropdown menu for `Potential paper cuts feedback?` column (see theme data to know if `Potential Papercuts` was selected)
* [ ] Create ad-hoc issue for PDI team to update USAT Tableau dashboards (see [example issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/1862 "[Ad Hoc Request] Replace Pilot Data in USAT Tableau Dashboard with FY25 Q2 Data"))
* [ ] Create a copy of a slide deck from the `GitLab User Satisfaction (USAT) Survey: FYXX QX` template (see [template folder](https://drive.google.com/drive/u/0/folders/1c03b1cyUP9mYX6TQffm1Bk2FTKkrltI1))
* [ ] Add results to slide deck
* [ ] Share draft report with UX Research team for peer review
* [ ] Make updates based on UX Research team feedback
* [ ] Add final results to `What did we learn?` section at the top of this issue
* [ ] Share out final report deck via Slack ( `#ux`, `#ux_research_reports`, `#product`, `#customer-success`, `@pmm-team`)
* [ ] Create new `USAT Responder Outreach` issue via template in `GitLab UX Research` project
  * [ ] Fill out information in the issue and add `Followup Users` sheet
  * [ ] Link `USAT Responder Outreach issue` to this issue
  * [ ] Message Product Managers, Product Designers, and Customer Success Managers about conducting follow up interviews with PCSAT survey respondents
* [ ] Update [USAT handbook page](https://handbook.gitlab.com/handbook/product/ux/performance-indicators/usat/) and [PCSAT issue template](https://gitlab.com/gitlab-org/ux-research/-/tree/master/.gitlab/issue_templates) to refine documentation
* [ ] Close this issue

# Resources

* [USAT Google Drive Folder (includes past data collection and templates)](https://drive.google.com/drive/u/0/folders/1ia-SJujFRWgOpnBGUdLE6gAWE8ldf0DM)
* [Customer Success / UX Survey Participants Google Drive Folder](https://drive.google.com/drive/folders/1ecM1ptkaNfNA0Im9wHulcl64QrCBOybQ)
* Google Sheets used:
  * FYXX QX Analysis Sheet
  * FYXX QX User List
  * FYXX QX All Verbatims
  * FYXX QX Follow Up Users

/label ~"UX research"
/confidential 
