<!-- 
This template is for internal and external Bashes. It's one of the main ways to convey the scope and format to participants, as well as provide links on how to submit their feedback and known issues of the feature. This template is a guide to help you get started, you should add or remove content based on your Bash goals.

For more information, see the [UX Bash handbook page](https://handbook.gitlab.com/handbook/product/ux/ux-research/ux-bash/)

Issue title format: [Feature name] Bash [YYYY-MM-DD] Guidelines and Instructions
 -->

## :triangular_ruler: Bash guidelines

**Objective:** [Example: Measure quality and identify bugs to improve the experience of Duo Chat in VS Code.]

**Scope:** [Example: Focus is on Duo Chat in VS Code.]

**Data Capture:** When reporting an interaction, include these details:

* [PROVIDE DETAILS FOR YOUR BASH]

<!-- Example:
* Your query: Provide the text you asked Duo.
* Duo's response: Provide the text of the response Duo generated for you.
* Ratings of interaction: Complete the rating scale questions of your interaction.
* Screenshots: Include a screenshot of your browser when you asked the question to give us an idea of your overall context.
-->

**Collaboration:** [Example: Use the dedicated Slack channel {link} / Use this dedicated issue to discuss bugs, seek clarifications, and share insights.]

**Timeline:** Will take place on **[ADD YOUR BASH DATE]**. Please report all interactions within this timeframe.

## :construction_worker: How to participate

To participate in this Bash, please follow these steps:

1. [ADD YOUR STEPS FOR PARTICIPATING]

<!-- Example:
1. Use Chat-in-IDE feature of Duo Chat: Pay attention to quality, functionality, user interface, performance, and any other relevant areas.
   - [How to access Chat-in-IDE](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/895#how-to-enable-the-chat-in-vscode)
2. [Report your interactions](https://gitlab.fra1.qualtrics.com/jfe/form/SV_8p2Hp3HVgsiVEKW): Create a new entry using the report template [here](https://gitlab.fra1.qualtrics.com/jfe/form/SV_8p2Hp3HVgsiVEKW) and provide all necessary details for every question you ask Duo.
3. Follow up: Once you've reported an interaction, keep an eye on the issue for any updates, feedback, or requests for additional information.
-->

## :exclamation: Known issues

As feedback comes in, this section will be updated to include known issues to avoid duplicate feedback and help you focus on bashing.

- Known issue 1
- …

---

Thank you for your participation in this Bash! Let's work together to improve our product's quality and deliver an exceptional user experience.