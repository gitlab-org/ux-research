<!--In the title, include the sections/stage groups you cover and the quarter/year for this issue.-->

<!--Please answer fill out the information below to the best of your ability. For details on this entire research prioritization process, please visit: https://handbook.gitlab.com/handbook/product/ux/ux-research/research-prioritization/ -->

<!--To see an example of a past prioritization issue, please visit: https://gitlab.com/gitlab-org/ux-research/-/issues/1793-->

### What is this issue about? (background and context)

This issue will contain a prioritized list of research projects and associated status updates for the projects that the {insert stage areas} UX Researcher will try to support during {insert quarter/year here}. 

More details on this process are provided in the [GitLab Handbook](https://handbook.gitlab.com/handbook/product/ux/ux-research/research-prioritization/). 

### How can I contribute to this issue?

1. Create a UX Research issue along with your request and link it within this issue in the comments. If you are unfamiliar with how to create a UX Research issue, please review the following [slides](https://docs.google.com/presentation/d/1ZWI1g3FC_9F6IRVdyZHYKeRAlQyFVXLoLSaRZg1Ro_g/edit#slide=id.g10ad1ea730e_0_22).

2. Fill out the [Research Prioritization Calculator](Add your sheet here)
<!-- When using the link to the Research Prioritization Calculator, please create a copy of this Google Sheet linked here (https://docs.google.com/spreadsheets/d/1RoT5YJ1g7mA09gMicKHobW54iykdqmB-3dYhhUqKjJ4/copy).

In the comments, include a link to this issue and share with stakeholders in your area (Product Designers, Product Design Managers and Product Managers) for the relevant group. If you're unsure of who that is, you can check here: https://about.gitlab.com/handbook/product/categories/ -->

### What research is happening right now? 
| Project Name | Research Issue | Support Level | Section or Stage Group | External Recruitment Needed? | Status | Status Details | Estimated Completion |
| ------ | ------ | ------ | ------ | ------  | ------ | ------  | ------  |
| ——— | ——— | ——— | ——— | ——— | ——— | ——— | ——— |
| ——— | ——— | ——— | ——— | ——— | ——— | ——— | ——— |
| ——— | ——— | ——— | ——— | ——— | ——— | ——— | ——— |

<!-- If you would like to split out projects by support level, please use the information within this comment in place of the section above.

### What research is happening right now? 

### Gold :first_place: Projects  

| Project Name | Research Issue | Support Level | Section or Stage Group | External Recruitment Needed? | Status | Status Details | Estimated Completion |
| ------ | ------ | ------ | ------ | ------  | ------ | ------  | ------  |
| ——— | ——— | ——— | ——— | ——— | ——— | ——— | ——— |

### Silver :second_place: Projects 

| Project Name | Research Issue | Support Level | Section or Stage Group | External Recruitment Needed? | Status | Status Details | Estimated Completion |
| ------ | ------ | ------ | ------ | ------  | ------ | ------  | ------  |
| ——— | ——— | ——— | ——— | ——— | ——— | ——— | ——— |

### Bronze :third_place: Projects 

| Project Name | Research Issue | Support Level | Section or Stage Group | External Recruitment Needed? | Status | Status Details | Estimated Completion |
| ------ | ------ | ------ | ------ | ------  | ------ | ------  | ------  |
| ——— | ——— | ——— | ——— | ——— | ——— | ——— | ——— |

-->

<!-- In the Estimated Completion column, you can provide either...

1) An estimated month and year when the research will be complete. Example: March 2022

Or

2) The time allotted for a project, time spent on a project, and number of days remaining on a project. Example: 5 days allotted, spent 2 days, 3 days remaining.

-->

### What level of support may we expect from our UXR?

| Support Level | Description | Scope |
| ------ | ------ | ------ |
| **Gold** :first_place: | **End-to-End Support:** UXR is the DRI, driving project management aspects of execution and will keep the project moving forward, but has support from PM/Design for milestone tasks - as delegated within each issue before a project is initiated. Task delegation is fixed with each team member committing to deliverables framed in the study plan during kick-off. Issues are used to formulate a plan, and then the UXR drives and executes. Keeping the team appraised but making key decisions about how to scope the project and next steps. | 0.5 - 2 active projects depending UX Researcher's level |
| **Silver** :second_place: | **Task-Specific Support:** PM/Design is the DRI, driving project management aspects of execution and completing mosts tasks, but has support from UXR. UXR provides dedicated support for specific tasks that take less than a few days to execute, but may need to deprioritize if needed to keep higher tier support projects in play. If that happens, team members need to pick up with that work. | 1 - 6 active projects (depending on UX Researcher's level | 
| **Bronze** :third_place: | **Consultant:** UXR advises on specific aspects of study planning and execution on an ad-hoc basis and during downtime between higher tier support. Here the UXR needs teams to tag them into issues, provide context and a due date for when feedback is needed by|  1-2 executed per quarter, 1-2 in planning, 1-2 in impact stage |

**_As issues are created for research projects, apply the corresponding support level label in the research issue._** 

#### Important information about research requests

Please be aware that adding comments/suggestions for research does not guarantee that your UXR will be able to support your request. We will do our best. However, there will be varying levels of support.

/label ~"UX Research Team"
/label ~"UX Research Prioritization" 
/confidential
