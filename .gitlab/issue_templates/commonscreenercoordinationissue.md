<!--
Title format: “Common Screener Coordination Issue: for H(#Half of year when the Common Screener will be used) (Month when recruit starts - Month when recruit ends)” Year Stage Groups
Example: “Common Screener Coordination Issue: Q3-Q4 (May - Jan) 2023 Verify & Package”
-->
 
<!--To learn about the Common Screener process please review the Recruiting handbook page at https://https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/common-screener/>-->


<!--**## To be completed by the requester**-->

#### What is this issue for?

We will use this issue to propose and draft a [common screener](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/common-screener/)  that we can use across studies to expedite our recruitment efforts.

#### Who are the target Personas for your proposed Common Screener
- 

<!--Example: 
*  [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer) 
*  [Ingrid (Infrastructure Operator)](https://about.gitlab.com/handbook/product/personas/#ingrid-infrastructure-operator) 
*  [Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer) 
*  [Sam (Security Analyst)](https://about.gitlab.com/handbook/product/personas/#sam-security-analyst) 
*  [Sidney (Systems Administrator)](https://about.gitlab.com/handbook/product/personas/#sidney-systems-administrator) 
*  [Rachel, Release Manager](https://about.gitlab.com/handbook/product/personas/#rachel-release-manager) 
*  [Allison, the Application Operator](https://about.gitlab.com/handbook/product/personas/#allison-application-ops) 
*  [Alex, Security Operations Engineer](https://about.gitlab.com/handbook/product/personas/#alex-security-operations-engineer) 
-->


#### What type of studies will use the Common Screener?
- 

<!--Example: 
*  [Problem Validation](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/problem-validation-and-methods/) 
* Foundational Research
* Solution Validation 
-->


#### What type of study formats would you like to recruit for
- 

<!--Example: 
* 30 min Zoom interviews
* 60 mins Zoom interviews
* 60 mins task based moderated usability studies
-->

#### In what GitLab product stages might your target users work?
- 

<!--Example: Example: 
* Verify
* Package
* Configure
* Release
* Secure
-->

#### What’s the earliest date that you’d like to be able to recruit participants from this Common Screener?
- 

<!--Example: 
* the last week of May 2022
-->

#### What’s the timeline during which you’d like to use this Common Screener?
- 

<!--Example: 
* the last week of May 2022
-->

#### **Checklist**

- [ ] Tag your UXR in this issue

- [ ] Does everyone who will user the screener understand that they must all create a dedicated recruitment request for each study where they’d like to use the common screener?

<!--Please note: We need a separate recruitment request for each study that uses a Common Screener so that we can track ReOps support hours. The benefit to you is that you'll be able to access participants much sooner than if you launched a separate request unrelated to this common screener-->

- [ ] Have you or someone on your team created a draft of a common screener in a document or in Qualtrics?  If yes, provide a link:   

- [ ] Have your teams and supporting UXRs had a chance to provide feedback to your draft of your common screener and confirmed that they’ll be able to use your common screener?

* [ ] [Make a copy of the Common Screener participant reimbursement spreadsheet](https://docs.google.com/spreadsheets/d/1OG1OUXLGvewvHq6VnnBB_7DZY7Bqv-nVCyPDQ_9idbg/edit?usp=sharing) and rename the copy to match the name of this Common Screener issue

<!-- Please use the following naming convention: <Common Screener Gratuity Spreadsheet for H(#Half of year when the Common Screener will be used) (Month when recruit starts - Month when recruit ends) Year Stage Groups Gratuity Spreadsheet-->

<!--Example: Common Screener H1 (Jan - July) 2023 Verify, Package, Release Gratuity Spreadsheet). -->

<!--Please note: You only need to have one spreadsheet for your Common Screener-->

* [ ] Move your copied sheet to the [UX Incentives Request](https://drive.google.com/drive/folders/1rfHKQbG4-X7COhIg33ycL6FlmEoIw0ZE) folder in the shared UX Research drive.

* [ ] Paste the link to the completed, copied spreadsheet here:

- [ ] Does everyone who will use the screener understand that they must use the Common Screener gratuity spreadsheet linked above?

/label ~”Backlog”

/label ~”UXR recruitment” ~”Research coordination” ~”ReOps:: Triage”

/confidential

/assign @cfaughnan @laurenevans
