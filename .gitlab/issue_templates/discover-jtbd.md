<!-- 
This is a template for discovering a stage group's main jobs using the Jobs to be Done (JTBD) Framework. If you're unfamiliar with the framework, please review the following handbook page(s) for more guidance.
- For learning more about JTBD: https://about.gitlab.com/handbook/product/ux/jobs-to-be-done/
- For learning more about using JTBD: https://about.gitlab.com/handbook/product/ux/jobs-to-be-done/using-jtbd/

-->

## What did we learn?

| Results               |
|-----------------------|
| `Job Performer`       |
| `Main Jobs`           |
| `1-3 Job Stories`     |
| FigJam link           |
| Dovetail Project link |

## Overarching goal of this research

The goal of this issue is to discover the Main Job(s) for `<Stage group>` and create a researched [JTBD Canvas](https://www.figma.com/file/kCbFiH1TptVbn8PMpjREFv/Jobs-to-be-done----Playbook-Template-(Copy)?type=whiteboard&node-id=0%3A1&t=WbAGq2z0LX0e6ZHL-1) for each Main Job.
   * If there are multiple jobs in the stage group, a new Canvas will need to be made in the same FigJam file. Copy a new instance of the entire "JTBD Canvas Playbook" container to a new section of your FigJam file and begin the Playbook process again.
   * For additional guidance on this process please read [How we do JTBD research at GitLab (A Playbook)](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/)

### [Workshop 1: Scope Definition](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#workshop-1-scope-definition)

This initial workshop should take about an hour to complete with the intention of defining the area, the Job Performer, and the Main Job you want to focus on for this JTBD Canvas. To get started, determine which of your counterparts you'll be workshopping with. At a minimum, this team should be comprised of the Product Design DRI and Product Manager. Other counterparts are definitley welcome. If this is your first time doing a JTBD workshop it will be beneficial to include a [JTBD Expert](https://about.gitlab.com/company/team/?department=product-design) (Cmd + F: "jobs to be done expert") to assist with facilitation.

**Workshoppers** (Whoever has the most experience with JTBD should act as the workshop facilitator.)
  * Product Designer: 
  * Product Manager: 
  * UX Researcher:
  * JTBD Expert / Facilitator (optional): 

**Workshop Date**: `yy-mm-dd | hh:mmAM/PM UTC`

**Workshop FigJam**: `FigJam link`

**Before Workshop**: (All workshoppers should read the following handbook pages.)
  * Read about [Jobs to be Done](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/)
  * Read about [How we do JTBD research at GitLab (A Playbook)](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/)


### [Pre-Workshop 2: Investigation Interviews & Interview Synthesis](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#pre-workshop-2-investigation-interviews--interview-synthesis)

#### Who will be moderating and/or assisting with interviews?

| Name | Timezone & ideal date range for interviews | Ideal number of interviews in that timeframe | Role(s) (moderator, note taker, both) |
|------|--------------------------------------------|----------------------------------------------|---------------------------------------|
| `<@namehere>` |  |  |  |
| `<@namehere>` |  |  |  |

Work with the UX researcher designated for your stage group to review and assist when needed: `<@namehere>`
<!-- Ideally, this person should have 1) read chapters 1-3 of Jim Kalbach's JTBD Playbook, 2) read all of GitLab's pages on JTBD, and 3) conducted JTBD interviews previously.

If you don't know someone who meets these requirements, ask the UX_Research slack channel for help. -->

#### Will anyone on this project be on PTO during this research? Which dates?

| Name        | OOO Dates            |
| ----------- | -------------------- |
| `@namehere` | yy-mm-dd to yy-mm-dd |
| `@namehere` | yy-mm-dd to yy-mm-dd |

**[Synthesize and Complete the JTBD Canvas](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#step-2-synthesize-and-complete-the-jtbd-canvas**

Now that the interviews are complete you'll need to sythesize all the information together to complete your JTBD Canvas.

Work with your counterparts to assist with data sythesis. Reach out to a [JTBD Expert](https://about.gitlab.com/company/team/?department=product-design) (Cmd + F: "jobs to be done expert") if you have questions about formatting or any other JTBD related questions: `<@namehere>`
<!-- Ideally, this person should have 1) read chapters 1-3 of Jim Kalbach's JTBD Playbook, 2) read all of GitLab's pages on JTBD, and 3) synthesized JTBD interviews previously.

If you don't know someone who meets these requirements, ask the UX_Research slack channel for help. -->

**Interview Synthesis** (Determine who will be available to synthesize interview data. This can be done async, sync, or a mix of both. Whatever time allows.)
  * Product Designer: 
  * Product Manager: 
  * UX Researcher:

### [Workshop 2: Map and Prioritize](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#workshop-2-map-and-prioritize)

For this final workshop, you should again, expect it to take about an hour to complete. The intention will be to select the most important aspects of each category of your JTBD Canvas. To get started, determine which of your counterparts you'll be workshopping with. It would be ideal if you could bring back the same group that participated in the first workshop, but at a minimum, this team should be comprised of the Product Design DRI and Product Manager. Again, other counterparts are definitley welcome. If this is your first time doing this JTBD workshop it will be beneficial to include a [JTBD Expert](https://about.gitlab.com/company/team/?department=product-design) (Cmd + F: "jobs to be done expert") to assist with facilitation. If you included a JTBD Expert in your first workshop, invite them back again for this workshop.

**Workshoppers** (Whoever has the most experience with JTBD should act as the workshop facilitator.)
  * Product Designer: 
  * Product Manager: 
  * UX Researcher:
  * JTBD Expert / Facilitator (optional): 

**Workshop Date**: `yy-mm-dd | hh:mmAM/PM UTC`

**Workshop FigJam**: `FigJam link` <!-- Should be the same FigJam link that's been used so far -->

#### [Outcome Opportunity Scores Survey](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#outcome-opportunity-scores-survey)

You may have noticed that you selected important aspects of every category on your JTBD Canvas but one, the Outcome Statements. In this case, you're going to rely on real users that represent your selected Job Performer to inform what's most important about your Outcome Statements. 

It's strongly recommended that you work with the UX researcher designated for your stage group to assist with setting up, recruiting, and finally analyzing the data of your Outcome Opportunity Score Survey: `<@namehere>`

#### [Generate Job Stories](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#generate-job-stories)

Generating Job Stories (formally we referred to these as Job Statements) is the final exercise in completing your JTBD Canvas. Job Stories are a great tool to help guide your team's next steps when solving problems for your users. It takes everything you just did to complete your canvases and puts it all together in easy to read stories that acts as summaries of the most important parts of your JTBD Canvas.

## Checklist / To Do

### Workshop 1: _(est. 1 hour)_

* [ ] Make a copy of the [JTBD Playbook FigJam](https://www.figma.com/file/Z4lsAOLH1ANN3pstQFYgSk/Jobs-to-be-done----Playbook-Template?type=whiteboard&node-id=219-2534&t=ubYbznDD1VrMzoIC-11) 
* [ ] Read [Jobs to be Done](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/)
* [ ] Read [How we do JTBD research at GitLab (A Playbook)](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/)
* [ ] Gather workshop participants and update this issue's description
* [ ] Schedule a date and time that aligns with workshop participants schedules and update this issue's description
* [ ] Complete Workshop 1

### Pre-Workshop 2: `Prepare for interviews` _(est. 1 week)_

* [ ] UX Researcher to ensure anyone planning on participating in interviews completes the [stakeholder availability section above](#who-will-be-moderating-andor-assisting-with-interviews)
* [ ] Create dovetail project  ([insert link to dovetail project]())
* [ ] Finalize interview guide (not a script) and screener
  * [ ] `<@namehere>` review screener
  * [ ] UX Researcher to ask UXR Team to review screener
* [ ] Create recruitment request issue ([insert link to issue]())
* [ ] Interview moderator(s) and notetaker(s) meet with a UX Researcher wtih JTBD experience. Ask the UX_Research [slack channel](https://gitlab.enterprise.slack.com/archives/CMEERUCE4) for help finding someone if needed.
  * [ ] First, review Assumptive JTBD Canvas from Workshop 1
  * [ ] Then, practice moderating JTBD interviews

#### `Conduct interviews` _(est. 2-3 weeks)_

* [ ] Begin recruitment effort and select participants
* [ ] Start interviews
* [ ] Use the space in your FigJam's JTBD Canvas to create Job Maps for each participant following [Exercise 1. Complete the Job Map](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#exercise-1-complete-the-job-map)
* [ ] Finish all interviews (around 5 - 10)
  * All stakeholders should feel confident in their Main Job by the end of the interviews, and participants should sound repetitive at this point.
* [ ] Put all interview recordings in Dovetail project.  ([insert link to dovetail project]())

#### `Sythesize interview data` _(est. 1 week)_

* [ ] Review the recordings and transcripts filling out each of the categories of your JTBD Canvas. Follow the exercises in the Pre-Workshop 2 section of your FIgJam for each participant.

### Workshop 2: _(est. 1 hour)_
* [ ] Gather workshop participants and update this issue's description
* [ ] Schedule a date and time that aligns with workshop participants schedules and update this issue's description
* [ ] Complete Workshop 2
* [ ] Copy over each top voted item into the appropriate category of your JTBD Canvas to the right of the exercises in your FigJam
* [ ] Preparte for your Outcome Opportunity Scores Survey
* [ ] Copy over your top prioritized Outcome Statements to your JTBD Canvas to the right of the exercises in your FigJam
* [ ] Write Job Stories using the top voted/prioritized items of your JTBD Canvas
* [ ] Put JTBD Canvas in the stage group's Figma folder
* [ ] Update the JTBD yml file for your Stage Group
* [ ] Add JTBD Canvas information to the [results section](#what-did-we-learn) of this issue
* [ ] Close issue

### Relevant supporting materials

* GitLab's JTBD [overview](https://about.gitlab.com/handbook/product/ux/jobs-to-be-done/) and [GitLab's JTBD Playbook](https://handbook.gitlab.com/handbook/product/ux/jobs-to-be-done/jtbd-playbook/#pre-workshop-2-investigation-interviews--interview-synthesis) handbook pages.
* [FigJam template](https://www.figma.com/file/Z4lsAOLH1ANN3pstQFYgSk/Jobs-to-be-done----Playbook-Template?type=whiteboard&node-id=219-2534&t=ubYbznDD1VrMzoIC-11) of GitLab's JTBD Playbook
* Jim Kalbach's [jtbdtoolkit.com](https://www.jtbdtoolkit.com/resource-library) or the [JTBD Playbook](https://a.co/d/02w3dod).
