## About
This project is used to propose research, track research projects and participant recruitment efforts in progress, and is used to document various UX Research team projects (like OKRs). To see past research and insights, log into [Dovetail](https://gitlab.dovetailapp.com/home). For those who do not currently have access to Dovetail, please submit an [Access Request](/handbook/business-technology/end-user-services/onboarding-access-requests/access-requests/) and review our handbook section on [getting started with Dovetail](/handbook/product/ux/dovetail/#getting-started-with-dovetail).

The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals. We use these insights to inform and strengthen product and design decisions.

## More infomation on UX Research at GitLab

The [UX Research Handbook](https://about.gitlab.com/handbook/engineering/ux/ux-research/) contains details about UX research, such as training materials, the methods we use at GitLab, participant recruitment, etc.


