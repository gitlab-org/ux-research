# Survey about requirements for monitoring – Research plan

## Background
During the research on Grafana usage, we've interviewed both internal 
and external team, some of the findings cause us to wonder if the requirements 
we collect from our infra team are not completely aligned with the requirements 
our targeted users need.

We are hoping that this survey will help us clarify this point.

**What hypotheses and/or assumptions do you have?**
The internal user's requirements from monitoring are different than the external ones

**What business decisions will be made based on this information?**
To what extend do we need to implement the requirements we are getting from our own infra team

## Research questions
- What are the feature requirements for dashboards, within the Metrics category?

- Are the requirements of external users different from requirements 
of our internal users?


## Method
- Survey 

### Data collection
- Tool: Qualtrics

### Data analysis
- According to Kano model

### Recruitment
Sample will be recruited from following groups:
- GitLab SRE team
- Developers and Operations professionals that don't use GitLab
- Users of Release, Monitor and Configure stages of GitLab


# Notes 
## SQL specification for 3.
TBD

### Examples: Simple – Only stage
```SQL
select
  user_id
from
  analytics.gitlab_dotcom_monthly_stage_active_users
where
  stage_name = 'monitor' and
  smau_month = '2020-05-01'
limit 100
```

### Example: More complicated** – Stage + user role
```SQL
select * 
from
  analytics.gitlab_dotcom_users_xf
right join
  analytics.gitlab_dotcom_monthly_stage_active_users
on
  analytics.gitlab_dotcom_users_xf.user_id = analytics.gitlab_dotcom_monthly_stage_active_users.user_id
where
  stage_name = 'monitor' and
    smau_month = '2020-05-01' and
  role = 'Software Developer'
limit 100
```

### Other options
- Limit on number of project members: 
     - So we filter out 1,2,5... people projects
     - Table: `analytics.gitlab_dotcom_projects_xf`
     - Parameter: `member_count`
- Account age: 
     - So we talk only to experienced GitLab users
     - Table: `analytics.gitlab_dotcom_users_xf`
     - Parameter: `account_age`
